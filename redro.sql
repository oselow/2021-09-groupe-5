-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : database
-- Généré le : mar. 22 fév. 2022 à 16:45
-- Version du serveur : 8.0.27
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `symfony_docker`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211026133330', '2022-01-28 11:09:26', 1337),
('DoctrineMigrations\\Version20211029104305', '2022-01-28 11:09:27', 521),
('DoctrineMigrations\\Version20211029105328', '2022-01-28 11:09:28', 113),
('DoctrineMigrations\\Version20211029105545', '2022-01-28 11:09:28', 133),
('DoctrineMigrations\\Version20211029142855', '2022-01-28 11:09:28', 81),
('DoctrineMigrations\\Version20211029143828', '2022-01-28 11:09:28', 357),
('DoctrineMigrations\\Version20211029154532', '2022-01-28 11:09:28', 42),
('DoctrineMigrations\\Version20211119090403', '2022-01-28 11:09:29', 93),
('DoctrineMigrations\\Version20211119092147', '2022-01-28 11:09:29', 145),
('DoctrineMigrations\\Version20211119111037', '2022-01-28 11:09:29', 156),
('DoctrineMigrations\\Version20211214143139', '2022-01-28 11:09:29', 58),
('DoctrineMigrations\\Version20211214144325', '2022-01-28 11:09:29', 32),
('DoctrineMigrations\\Version20220129191856', '2022-01-31 20:20:18', 62),
('DoctrineMigrations\\Version20220222153046', '2022-02-22 15:31:08', 161);

-- --------------------------------------------------------

--
-- Structure de la table `food`
--

CREATE TABLE `food` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `food`
--

INSERT INTO `food` (`id`, `name`, `price`) VALUES
(28, 'pates carbo', 5.85),
(29, 'pates saumon', 6.95),
(30, 'coca', 2);

-- --------------------------------------------------------

--
-- Structure de la table `food_menu`
--

CREATE TABLE `food_menu` (
  `food_id` int NOT NULL,
  `menu_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `food_menu`
--

INSERT INTO `food_menu` (`food_id`, `menu_id`) VALUES
(28, 19),
(29, 20),
(30, 19),
(30, 20);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_unite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_red_meat` tinyint(1) NOT NULL DEFAULT '0',
  `is_white_meat` tinyint(1) NOT NULL DEFAULT '0',
  `is_fish` tinyint(1) NOT NULL DEFAULT '0',
  `is_vegeteble` tinyint(1) NOT NULL DEFAULT '0',
  `is_starchy` tinyint(1) NOT NULL DEFAULT '0',
  `is_dairy` tinyint(1) NOT NULL DEFAULT '0',
  `is_peanut` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `name`, `default_unite`, `is_red_meat`, `is_white_meat`, `is_fish`, `is_vegeteble`, `is_starchy`, `is_dairy`, `is_peanut`) VALUES
(46, 'lardon', 'g', 0, 1, 0, 0, 0, 0, 0),
(47, 'saumon', 'g', 0, 0, 1, 0, 0, 0, 0),
(48, 'pates', 'g', 0, 0, 0, 0, 1, 0, 0),
(49, 'creme fraiche', 'ml', 0, 0, 0, 0, 0, 1, 0),
(50, 'coca', NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient_quantity`
--

CREATE TABLE `ingredient_quantity` (
  `id` int NOT NULL,
  `food_id` int NOT NULL,
  `ingredient_id` int NOT NULL,
  `quantity` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ingredient_quantity`
--

INSERT INTO `ingredient_quantity` (`id`, `food_id`, `ingredient_id`, `quantity`) VALUES
(64, 28, 48, 193.5),
(65, 28, 49, 145.5),
(66, 28, 46, 172.2),
(67, 29, 48, 120.4),
(68, 29, 49, 154.6),
(69, 29, 47, 180),
(70, 30, 50, 144.9);

-- --------------------------------------------------------

--
-- Structure de la table `macro`
--

CREATE TABLE `macro` (
  `id` int NOT NULL,
  `ingredient_id` int NOT NULL,
  `proteines` double DEFAULT NULL,
  `glucides` double DEFAULT NULL,
  `lipides` double DEFAULT NULL,
  `calories` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `macro`
--

INSERT INTO `macro` (`id`, `ingredient_id`, `proteines`, `glucides`, `lipides`, `calories`) VALUES
(46, 46, 1, 0.7, 1.8, 1),
(47, 47, 2, 1.2, 1.2, 1.6),
(48, 48, 1.4, 2, 1.6, 1.6),
(49, 49, 2, 1.1, 2, 0.7),
(50, 50, 0.5, 1.2, 0.8, 0.8);

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`id`, `name`, `price`, `is_active`) VALUES
(19, 'Menu du chef', 16.5, 1),
(20, 'Menu aquatique', 11, 1);

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE `order` (
  `id` int NOT NULL,
  `reservation_id` int DEFAULT NULL,
  `menu_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `order`
--

INSERT INTO `order` (`id`, `reservation_id`, `menu_id`, `food_id`, `comment`, `status`) VALUES
(369, 114, NULL, 29, NULL, 'P'),
(378, 114, NULL, 29, NULL, 'P'),
(379, 112, NULL, 30, NULL, 'C'),
(380, 114, NULL, 28, NULL, NULL),
(385, 114, NULL, 30, NULL, 'C'),
(387, 112, 19, NULL, NULL, NULL),
(388, 111, NULL, 28, NULL, 'P'),
(389, 114, NULL, 28, NULL, NULL),
(390, 111, NULL, 28, NULL, 'P'),
(397, 111, 19, NULL, NULL, 'P');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id`, `start_date`, `end_date`, `name`) VALUES
(111, '2022-02-18 13:30:00', '2022-02-18 15:30:00', 'Pinto Laurence'),
(112, '2022-02-24 13:00:00', '2022-02-24 14:30:00', 'Tanguy Lorraine'),
(114, '2022-02-23 12:30:00', '2022-02-23 15:00:00', 'Bernier Anne');

-- --------------------------------------------------------

--
-- Structure de la table `reservation_restaurant_table`
--

CREATE TABLE `reservation_restaurant_table` (
  `reservation_id` int NOT NULL,
  `restaurant_table_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reservation_restaurant_table`
--

INSERT INTO `reservation_restaurant_table` (`reservation_id`, `restaurant_table_id`) VALUES
(111, 183),
(111, 193),
(112, 184),
(112, 194),
(114, 186);

-- --------------------------------------------------------

--
-- Structure de la table `restaurant_table`
--

CREATE TABLE `restaurant_table` (
  `id` int NOT NULL,
  `max_capacity` int DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `restaurant_table`
--

INSERT INTO `restaurant_table` (`id`, `max_capacity`, `is_active`) VALUES
(183, 4, 1),
(184, 10, 0),
(186, 4, 1),
(193, 5, 0),
(194, 9, 0),
(195, 5, 1),
(196, 8, 1),
(197, 8, 1),
(198, 8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `roles`, `password`) VALUES
(100, 'test0@gmail.com', 'test0', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(101, 'test1@gmail.com', 'test1', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(102, 'test2@gmail.com', 'test2', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(103, 'test3@gmail.com', 'test3', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(104, 'test4@gmail.com', 'test4', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(105, 'test5@gmail.com', 'test5', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(106, 'test6@gmail.com', 'test6', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(107, 'test7@gmail.com', 'test7', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(108, 'test8@gmail.com', 'test8', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(109, 'test9@gmail.com', 'test9', '[\"ROLE_GESTIONNAIRE\"]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2'),
(110, 'admin@gmail.com', 'admin', '[]', '$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `food_menu`
--
ALTER TABLE `food_menu`
  ADD PRIMARY KEY (`food_id`,`menu_id`),
  ADD KEY `IDX_C8229242BA8E87C4` (`food_id`),
  ADD KEY `IDX_C8229242CCD7E912` (`menu_id`);

--
-- Index pour la table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ingredient_quantity`
--
ALTER TABLE `ingredient_quantity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EDF546B8BA8E87C4` (`food_id`),
  ADD KEY `IDX_EDF546B8933FE08C` (`ingredient_id`);

--
-- Index pour la table `macro`
--
ALTER TABLE `macro`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_AE8FC643933FE08C` (`ingredient_id`);

--
-- Index pour la table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5299398B83297E7` (`reservation_id`),
  ADD KEY `IDX_F5299398CCD7E912` (`menu_id`),
  ADD KEY `IDX_F5299398BA8E87C4` (`food_id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation_restaurant_table`
--
ALTER TABLE `reservation_restaurant_table`
  ADD PRIMARY KEY (`reservation_id`,`restaurant_table_id`),
  ADD KEY `IDX_B36BD51FB83297E7` (`reservation_id`),
  ADD KEY `IDX_B36BD51FCC5AE6B3` (`restaurant_table_id`);

--
-- Index pour la table `restaurant_table`
--
ALTER TABLE `restaurant_table`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `food`
--
ALTER TABLE `food`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `ingredient_quantity`
--
ALTER TABLE `ingredient_quantity`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT pour la table `macro`
--
ALTER TABLE `macro`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `order`
--
ALTER TABLE `order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT pour la table `restaurant_table`
--
ALTER TABLE `restaurant_table`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `food_menu`
--
ALTER TABLE `food_menu`
  ADD CONSTRAINT `FK_C8229242BA8E87C4` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C8229242CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ingredient_quantity`
--
ALTER TABLE `ingredient_quantity`
  ADD CONSTRAINT `FK_EDF546B8933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `FK_EDF546B8BA8E87C4` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`);

--
-- Contraintes pour la table `macro`
--
ALTER TABLE `macro`
  ADD CONSTRAINT `FK_AE8FC643933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`);

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F5299398B83297E7` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F5299398BA8E87C4` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`),
  ADD CONSTRAINT `FK_F5299398CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`);

--
-- Contraintes pour la table `reservation_restaurant_table`
--
ALTER TABLE `reservation_restaurant_table`
  ADD CONSTRAINT `FK_B36BD51FB83297E7` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B36BD51FCC5AE6B3` FOREIGN KEY (`restaurant_table_id`) REFERENCES `restaurant_table` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
