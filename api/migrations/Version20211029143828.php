<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211029143828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reservation_restaurant_table (reservation_id INT NOT NULL, restaurant_table_id INT NOT NULL, INDEX IDX_B36BD51FB83297E7 (reservation_id), INDEX IDX_B36BD51FCC5AE6B3 (restaurant_table_id), PRIMARY KEY(reservation_id, restaurant_table_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reservation_restaurant_table ADD CONSTRAINT FK_B36BD51FB83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation_restaurant_table ADD CONSTRAINT FK_B36BD51FCC5AE6B3 FOREIGN KEY (restaurant_table_id) REFERENCES restaurant_table (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE reservation_restaurant_table');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
