<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214144325 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient CHANGE is_red_meat is_red_meat TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_white_meat is_white_meat TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_fish is_fish TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_vegeteble is_vegeteble TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_starchy is_starchy TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_dairy is_dairy TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE is_peanut is_peanut TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient CHANGE is_red_meat is_red_meat TINYINT(1) NOT NULL, CHANGE is_white_meat is_white_meat TINYINT(1) NOT NULL, CHANGE is_fish is_fish TINYINT(1) NOT NULL, CHANGE is_vegeteble is_vegeteble TINYINT(1) NOT NULL, CHANGE is_starchy is_starchy TINYINT(1) NOT NULL, CHANGE is_dairy is_dairy TINYINT(1) NOT NULL, CHANGE is_peanut is_peanut TINYINT(1) NOT NULL');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
