<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214143139 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient ADD is_red_meat TINYINT(1) NOT NULL, ADD is_white_meat TINYINT(1) NOT NULL, ADD is_fish TINYINT(1) NOT NULL, ADD is_vegeteble TINYINT(1) NOT NULL, ADD is_starchy TINYINT(1) NOT NULL, ADD is_dairy TINYINT(1) NOT NULL, ADD is_peanut TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP is_red_meat, DROP is_white_meat, DROP is_fish, DROP is_vegeteble, DROP is_starchy, DROP is_dairy, DROP is_peanut');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
