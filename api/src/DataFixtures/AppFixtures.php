<?php

namespace App\DataFixtures;

use App\Entity\Food;
use App\Entity\IngredientQuantity;
use App\Entity\Menu;
use App\Entity\Order;
use App\Entity\Reservation;
use App\Entity\RestaurantTable;
use App\Entity\User;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Macro;
use App\Entity\Ingredient;
use Exception;
use Faker;

class AppFixtures extends Fixture
{
    private $manager = null;
    private $faker = null;

    private $macros = array();
    private $ingredients = array();
    private $food = array();
    private $ingredientsQuantities = array();
    private $menus = array();
    private $restaurentTables = array();
    private $reservations = array();
    private $orders = array();
    private $users = array();
    private $container;

    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
    }

    public function load(ObjectManager $manager)
    {
        date_default_timezone_set('Europe/Paris');

        $this->manager = $manager;
        $this->faker = Faker\Factory::create('fr_FR');

        $this->fakeIngredients();
        $this->fakeMacros();
        $this->fakeFood();
        $this->fakeIngredientQuantities();
        $this->fakeMenus();
        $this->fakeRestaurentTables();
        $this->fakeReservations();
        $this->fakeOrders();

        $this->fakeSomeUsers("test");
        $this->fakeUser("admin");

        $this->manager->flush();
    }


    public function fakeMacros()
    {
        foreach ($this->ingredients as $ingredient) {
            array_push($this->macros, new Macro());
            $number = sizeof($this->macros) - 1;
            $this->macros[$number]->setIngredient($ingredient);
            $this->macros[$number]->setProteines((rand(5, 20) / 10));
            $this->macros[$number]->setGlucides((rand(5, 20) / 10));
            $this->macros[$number]->setLipides((rand(5, 20) / 10));
            $this->macros[$number]->setCalories((rand(5, 20) / 10));
            $this->manager->persist($this->macros[$number]);
        }
    }

    public function fakeIngredients()
    {
        $this->generateIngredient("lardon", "g", ["isWhiteMeat"]);
        $this->generateIngredient("saumon", "g", ["isFish"]);
        $this->generateIngredient("pates", "g", ["isStarchy"]);
        $this->generateIngredient("creme fraiche", "ml", ["isDairy"]);
        $this->generateIngredient("coca");
    }

    public function generateIngredient($name, $defaultUnite = null, $filters = [])
    {
        array_push($this->ingredients, new Ingredient());
        $number = sizeof($this->ingredients) - 1;
        $this->ingredients[$number]->setName($name);
        $this->ingredients[$number]->setDefaultUnite($defaultUnite);

        foreach ($filters as $filter) {
            switch ($filter) {
                case "isWhiteMeat" :
                    $this->ingredients[$number]->setIsWhiteMeat(true);
                    break;
                case "isFish" :
                    $this->ingredients[$number]->setIsFish(true);
                    break;
                case "isStarchy" :
                    $this->ingredients[$number]->setIsStarchy(true);
                    break;
                case "isDairy" :
                    $this->ingredients[$number]->setIsDairy(true);
                    break;
                default:
                    break;
            }
        }

        $this->manager->persist($this->ingredients[$number]);
    }

    public function fakeFood()
    {
        $this->generateFood("pates carbo", 5.85);
        $this->generateFood("pates saumon", 6.95);
        $this->generateFood("coca", 2);
    }

    public function generateFood($name, $price)
    {
        array_push($this->food, new Food());
        $number = sizeof($this->food) - 1;
        $this->food[$number]->setName($name);
        $this->food[$number]->setPrice($price);
        $this->manager->persist($this->food[$number]);
    }

    public function fakeIngredientQuantities()
    {
        $food = array_filter($this->food, function ($f) {
            return $f->getName() === "pates carbo";
        });
        $this->generateQuantities(
            $food,
            [
                ["name" => "pates", "quantity" => (rand(1000, 2000) / 10)],
                ["name" => "creme fraiche", "quantity" => (rand(1000, 2000) / 10)],
                ["name" => "lardon", "quantity" => (rand(1000, 2000) / 10)],
            ]
        );

        $food = array_filter($this->food, function ($f) {
            return $f->getName() === "pates saumon";
        });
        $this->generateQuantities(
            $food,
            [
                ["name" => "pates", "quantity" => (rand(1000, 2000) / 10)],
                ["name" => "creme fraiche", "quantity" => (rand(1000, 2000) / 10)],
                ["name" => "saumon", "quantity" => (rand(1000, 2000) / 10)],
            ]
        );

        $food = array_filter($this->food, function ($f) {
            return $f->getName() === "coca";
        });
        $this->generateQuantities(
            $food,
            [
                ["name" => "coca", "quantity" => (rand(1000, 2000) / 10)]
            ]
        );
    }

    public function generateQuantities($food, $ingredientQuantities)
    {
        foreach ($ingredientQuantities as $ingredientQuantity) {
            $name = $ingredientQuantity["name"];
            $quantity = $ingredientQuantity["quantity"];
            array_push($this->ingredientsQuantities, new IngredientQuantity());
            $number = sizeof($this->ingredientsQuantities) - 1;
            $ingredient = array_filter($this->ingredients, function ($f) use ($name) {
                return $f->getName() === $name;
            });
            $this->ingredientsQuantities[$number]->setFood(array_values($food)[0]);
            $this->ingredientsQuantities[$number]->setIngredient(array_values($ingredient)[0]);
            $this->ingredientsQuantities[$number]->setQuantity($quantity);
            $this->manager->persist($this->ingredientsQuantities[$number]);
        }
    }

    public function fakeMenus()
    {
        $this->generateMenus(
            "Menu du chef",
            [
                array_filter($this->food, function ($f) {
                    return $f->getName() === "pates carbo";
                }),
                array_filter($this->food, function ($f) {
                    return $f->getName() === "coca";
                })
            ],
            (rand(100, 200) / 10)
        );

        $this->generateMenus(
            "Menu aquatique",
            [
                array_filter($this->food, function ($f) {
                    return $f->getName() === "pates saumon";
                }),
                array_filter($this->food, function ($f) {
                    return $f->getName() === "coca";
                })
            ],
            (rand(100, 200) / 10)
        );
    }

    public function generateMenus($name, $foods, $price)
    {
        array_push($this->menus, new Menu());
        $number = sizeof($this->menus) - 1;

        $this->menus[$number]->setName($name);
        foreach ($foods as $food) {
            $this->menus[$number]->addFood(array_shift($food));
        }
        $this->menus[$number]->setPrice($price);
        $this->manager->persist($this->menus[$number]);
    }

    public function fakeRestaurentTables()
    {
        for ($i = 0; $i < 20; $i++) {
            array_push($this->restaurentTables, new RestaurantTable());
            $number = sizeof($this->restaurentTables) - 1;
            $this->restaurentTables[$number]->setMaxCapacity(rand(2, 10));
            $this->restaurentTables[$number]->setIsActive((bool)rand(0, 1));
            $this->manager->persist($this->restaurentTables[$number]);
        }
    }

    /**
     * @throws Exception
     */
    public function fakeReservations()
    {
        for ($i = 0; $i < 10; $i++) {
            array_push($this->reservations, new Reservation());
            $number = sizeof($this->reservations) - 1;
            $start = $this->faker->dateTimeBetween('-10 days', '+10 days');
            $startHour = rand(12, 14);
            $startMinute = rand(0, 1) === 0 ? 00 : 30;
            date_time_set($start, $startHour, $startMinute, 00);
            $end = (clone $start)->add(new DateInterval("PT" . rand(1, 2) . "H"));
            date_time_set($end, $startHour + rand(1, 2), $startMinute + (rand(0, 1) === 0 ? 00 : 30), 00);
            $this->reservations[$number]->setName($this->faker->lastName . ' ' . $this->faker->firstname);
            $this->reservations[$number]->setStartDate($start);
            $this->reservations[$number]->setEndDate($end);
            $this->reservations[$number]->addRestaurantTable($this->restaurentTables[$number]);
            if ((bool)rand(0, 1)) //ajoute aléatoirement une deuxieme table à la reservation
                $this->reservations[$number]->addRestaurantTable($this->restaurentTables[$number + 10]);
            $this->manager->persist($this->reservations[$number]);
        }

        array_push($this->reservations, new Reservation());
        $number = sizeof($this->reservations) - 1;
        $start = new DateTime('NOW');
        $end = (clone $start)->add(new DateInterval("PT" . rand(1, 2) . "H"));
        $this->reservations[$number]->setName("gillot");
        $this->reservations[$number]->setStartDate($start);
        $this->reservations[$number]->setEndDate($end);
        $this->reservations[$number]->addRestaurantTable($this->restaurentTables[$number - 1]);
        $this->reservations[$number]->addRestaurantTable($this->restaurentTables[$number]);
        $this->manager->persist($this->reservations[$number]);

        array_push($this->reservations, new Reservation());
        $number = sizeof($this->reservations) - 1;
        $start = new DateTime('NOW', new DateTimeZone('Europe/Paris'));
        $end = (clone $start)->add(new DateInterval("PT" . rand(1, 2) . "H"));
        $this->reservations[$number]->setName("gillot");
        $this->reservations[$number]->setStartDate($start);
        $this->reservations[$number]->setEndDate($end);
        $this->reservations[$number]->addRestaurantTable($this->restaurentTables[$number - 2]);
        $this->manager->persist($this->reservations[$number]);
    }

    public function fakeOrders()
    {
        for ($i = 0; $i < 40; $i++) {
            $status = match (rand(1, 4)) {
                2 => "C",
                3 => "A",
                4 => "P",
                default => null,
            };

            array_push($this->orders, new Order());
            $number = sizeof($this->orders) - 1;
            $this->orders[$number]->setReservation($this->reservations[rand(0, sizeof($this->reservations) - 1)]);
            $this->orders[$number]->setStatus($status);
            if ((bool)rand(0, 1)) //ajoute aléatoirement un menu ou un article à part
                $this->orders[$number]->setMenu($this->menus[rand(0, sizeof($this->menus) - 1)]);
            else
                $this->orders[$number]->setFood($this->food[rand(0, sizeof($this->food) - 1)]);
            $this->manager->persist($this->orders[$number]);
        }
    }

    public function fakeSomeUsers($name)
    {
        for ($i = 0; $i < 10; $i++) {
            array_push($this->users, new User());
            $number = sizeof($this->users) - 1;
            $this->users[$number]->setUsername($name . $i);
            $this->users[$number]->setEmail($name . $i . '@gmail.com');
            $this->users[$number]->setRoles(["ROLE_GESTIONNAIRE"]);
            $this->users[$number]->setPassword('$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2');
            $this->manager->persist($this->users[$number]);
        }
    }

    public function fakeUser($name)
    {
        array_push($this->users, new User());
        $number = sizeof($this->users) - 1;
        $this->users[$number]->setUsername($name);
        $this->users[$number]->setEmail($name . '@gmail.com');
        $this->users[$number]->setPassword('$2y$13$W3pwTlLmLdaKpDOfVn1xVu.SR7LSfyenTKbSL6SaROiec5mVmd5C2');
        $this->manager->persist($this->users[$number]);
    }
}
