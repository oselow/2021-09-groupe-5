<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FoodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FoodRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['food:write']],
    normalizationContext: ['groups' => ['food:read']],
)]
class Food
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(["menu:read", "food:read", "food:write", "ingredientQuantities:read", "order:read", "restaurantTable:read"])]
    private $id;

    #[ORM\Column(type: "string", length: 255)]
    #[Groups(["menu:read", "food:read", "food:write", "ingredientQuantities:read", "order:read", "restaurantTable:read"])]
    private $name;

    #[ORM\Column(type: "float")]
    #[Groups(["food:read", "food:write", "ingredientQuantities:read", "order:read", "restaurantTable:read"])]
    private $price;

    #[ORM\ManyToMany(targetEntity: Menu::class, inversedBy: "foods")]
    #[Groups(["ingredientQuantities:read"])]
    private $menus;

    #[ORM\OneToMany(mappedBy: 'food', targetEntity: IngredientQuantity::class, orphanRemoval: true)]
    #[Groups(["menu:read", "food:read", "food:write"])]
    private $ingredientQuantities;

    #[ORM\OneToMany(mappedBy: 'food', targetEntity: Order::class)]
    private $orders;

    // Filter from ingredient if almost one is true
    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isRedMeat;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isWhiteMeat;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isFish;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isVegeteble;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isStarchy;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isDairy;

    #[Groups(["menu:read", "food:read", "food:write"])]
    private $isPeanut;

    public function __construct()
    {
        $this->menu = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->ingredientQuantities = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        $this->menus->removeElement($menu);

        return $this;
    }

    /**
     * @return Collection|IngredientQuantity[]
     */
    public function getIngredientQuantities(): Collection
    {
        return $this->ingredientQuantities;
    }

    public function addIngredientQuantity(IngredientQuantity $ingredientQuantity): self
    {
        if (!$this->ingredientQuantities->contains($ingredientQuantity)) {
            $this->ingredientQuantities[] = $ingredientQuantity;
            $ingredientQuantity->setFood($this);
        }

        return $this;
    }

    public function removeIngredientQuantity(IngredientQuantity $ingredientQuantity): self
    {
        if ($this->ingredientQuantities->removeElement($ingredientQuantity)) {
            // set the owning side to null (unless already changed)
            if ($ingredientQuantity->getFood() === $this) {
                $ingredientQuantity->setFood(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setFood($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getFood() === $this) {
                $order->setFood(null);
            }
        }

        return $this;
    }

    public function getIsRedMeat(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsRedMeat())
                return true;
        }

        return false;
    }

    public function getIsWhiteMeat(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsWhiteMeat())
                return true;
        }

        return false;
    }

    public function getIsFish(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsFish())
                return true;
        }

        return false;
    }

    public function getIsVegeteble(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsVegeteble())
                return true;
        }

        return false;
    }

    public function getIsStarchy(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsStarchy())
                return true;
        }

        return false;
    }

    public function getIsDairy(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsDairy())
                return true;
        }

        return false;
    }

    public function getIsPeanut(): bool
    {
        foreach ($this->getIngredientQuantities() as $ingredientQuantities){
            if ($ingredientQuantities->getIngredient()->getIsPeanut())
                return true;
        }

        return false;
    }
}
