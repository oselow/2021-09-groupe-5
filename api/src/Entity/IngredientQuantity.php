<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\IngredientQuantityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: IngredientQuantityRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['ingredientQuantities:write']],
    normalizationContext: ['groups' => ['ingredientQuantities:read']],
)]
class IngredientQuantity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["menu:read", "ingredientQuantities:read", "ingredientQuantities:write"])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Food::class, inversedBy: 'ingredientQuantities')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["ingredientQuantities:read", "ingredientQuantities:write"])]
    private $food;

    #[ORM\ManyToOne(targetEntity: Ingredient::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["menu:read", "food:read", "ingredientQuantities:read", "ingredientQuantities:write"])]
    private $ingredient;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(["menu:read", "food:read", "ingredientQuantities:read", "ingredientQuantities:write"])]
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFood(): ?Food
    {
        return $this->food;
    }

    public function setFood(?Food $food): self
    {
        $this->food = $food;

        return $this;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(?Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
