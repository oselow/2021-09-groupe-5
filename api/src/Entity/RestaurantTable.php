<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Repository\RestaurantTableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass:RestaurantTableRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['restaurantTable:write']],
    normalizationContext: ['groups' => ['restaurantTable:read']],
)]
#[ApiFilter(BooleanFilter::class, properties: ['isActive'])]
class RestaurantTable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type:"integer")]
    #[Groups(["reservation:read", "restaurantTable:write", "restaurantTable:read", "order:read"])]
    private $id;

    #[ORM\Column(type:"integer", nullable:true)]
    #[Groups(["reservation:read", "restaurantTable:write", "restaurantTable:read", "order:read"])]
    private $maxCapacity;

    #[ORM\Column(type: 'boolean')]
    #[Groups(["reservation:read", "restaurantTable:write", "restaurantTable:read", "order:read"])]
    private $isActive = true;

    #[ORM\ManyToMany(targetEntity: Reservation::class, mappedBy: 'restaurantTable',  orphanRemoval:true, cascade:['remove'])]
    #[ORM\JoinColumn(onDelete:'CASCADE' )]
    #[Groups(["restaurantTable:write", "restaurantTable:read"])]
    private $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }

    public function setMaxCapacity(?int $maxCapacity): self
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->addRestaurantTable($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            $reservation->removeRestaurantTable($this);
        }

        return $this;
    }
}
