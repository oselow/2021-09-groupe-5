<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\IngredientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass:IngredientRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['ingredient:write']],
    normalizationContext: ['groups' => ['ingredient:read']],
)]
class Ingredient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type:"integer")]
    #[Groups(["menu:read", "food:read", "ingredient:read", "ingredient:write", "ingredientQuantities:read", "macro:read"])]
    private $id;

    #[ORM\Column(type:"string", length:255)]
    #[Groups(["menu:read", "food:read", "ingredient:read", "ingredient:write", "ingredientQuantities:read", "macro:read"])]
    private $name;

    #[ORM\OneToOne(mappedBy: 'ingredient', targetEntity: Macro::class, cascade: ['persist', 'remove'])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read", "food:read"])]
    private $macro;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(["menu:read", "food:read", "ingredient:read", "ingredient:write", "ingredientQuantities:read", "macro:read"])]
    private $defaultUnite = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isRedMeat = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isWhiteMeat = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isFish = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isVegeteble = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isStarchy = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isDairy = false;

    #[ORM\Column(type: 'boolean', options: ["default" => 0])]
    #[Groups(["ingredient:read", "ingredient:write", "menu:read"])]
    private $isPeanut = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMacro(): ?Macro
    {
        return $this->macro;
    }

    public function setMacro(Macro $macro): self
    {
        // set the owning side of the relation if necessary
        if ($macro->getIngredient() !== $this) {
            $macro->setIngredient($this);
        }

        $this->macro = $macro;

        return $this;
    }

    public function getDefaultUnite(): ?string
    {
        return $this->defaultUnite;
    }

    public function setDefaultUnite(?string $defaultUnite): self
    {
        $this->defaultUnite = $defaultUnite;

        return $this;
    }

    public function getIsRedMeat(): ?bool
    {
        return $this->isRedMeat;
    }

    public function setIsRedMeat(bool $isRedMeat): self
    {
        $this->isRedMeat = $isRedMeat;

        return $this;
    }

    public function getIsWhiteMeat(): ?bool
    {
        return $this->isWhiteMeat;
    }

    public function setIsWhiteMeat(bool $isWhiteMeat): self
    {
        $this->isWhiteMeat = $isWhiteMeat;

        return $this;
    }

    public function getIsFish(): ?bool
    {
        return $this->isFish;
    }

    public function setIsFish(bool $isFish): self
    {
        $this->isFish = $isFish;

        return $this;
    }

    public function getIsVegeteble(): ?bool
    {
        return $this->isVegeteble;
    }

    public function setIsVegeteble(bool $isVegeteble): self
    {
        $this->isVegeteble = $isVegeteble;

        return $this;
    }

    public function getIsStarchy(): ?bool
    {
        return $this->isStarchy;
    }

    public function setIsStarchy(bool $isStarchy): self
    {
        $this->isStarchy = $isStarchy;

        return $this;
    }

    public function getIsDairy(): ?bool
    {
        return $this->isDairy;
    }

    public function setIsDairy(bool $isDairy): self
    {
        $this->isDairy = $isDairy;

        return $this;
    }

    public function getIsPeanut(): ?bool
    {
        return $this->isPeanut;
    }

    public function setIsPeanut(bool $isPeanut): self
    {
        $this->isPeanut = $isPeanut;

        return $this;
    }
}
