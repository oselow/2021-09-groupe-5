<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MacroRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass:MacroRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['macro:write']],
    normalizationContext: ['groups' => ['macro:read']],
)]
class Macro
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type:"integer")]
    #[Groups(["ingredient:read", "macro:read", "macro:write", "menu:read", "food:read"])]
    private $id;

    #[ORM\OneToOne(inversedBy: 'macro', targetEntity: Ingredient::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["macro:read", "macro:write"])]
    private $ingredient;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(["ingredient:read", "macro:read", "macro:write", "menu:read", "food:read"])]
    private $proteines;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(["ingredient:read", "macro:read", "macro:write", "menu:read", "food:read"])]
    private $glucides;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(["ingredient:read", "macro:read", "macro:write", "menu:read", "food:read"])]
    private $lipides;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(["ingredient:read", "macro:read", "macro:write", "menu:read", "food:read"])]
    private $calories;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function getProteines(): ?float
    {
        return $this->proteines;
    }

    public function setProteines(?float $proteines): self
    {
        $this->proteines = $proteines;

        return $this;
    }

    public function getGlucides(): ?float
    {
        return $this->glucides;
    }

    public function setGlucides(?float $glucides): self
    {
        $this->glucides = $glucides;

        return $this;
    }

    public function getLipides(): ?float
    {
        return $this->lipides;
    }

    public function setLipides(?float $lipides): self
    {
        $this->lipides = $lipides;

        return $this;
    }

    public function getCalories(): ?float
    {
        return $this->calories;
    }

    public function setCalories(?float $calories): self
    {
        $this->calories = $calories;

        return $this;
    }
}
