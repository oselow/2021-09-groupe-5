<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['reservation:write']],
    normalizationContext: ['groups' => ['reservation:read']],
)]
#[ApiFilter(DateFilter::class, properties: ['startDate','endDate'])]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'iexact', 'restaurantTable.id' => 'iexact'])]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    #[Groups(["reservation:read", "reservation:write", "restaurantTable:read", "order:read"])]
    private $id;

    #[ORM\Column(type: "datetime")]
    #[Groups(["reservation:read", "reservation:write", "restaurantTable:read", "order:read"])]
    private $startDate;

    #[ORM\Column(type: "datetime", nullable: true)]
    #[Groups(["reservation:read", "reservation:write", "restaurantTable:read", "order:read"])]
    private $endDate;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: Order::class, orphanRemoval:true, cascade:['persist', 'remove'])]
    #[ORM\JoinColumn(onDelete:'CASCADE')]
    #[Groups(["reservation:read", "reservation:write", "restaurantTable:read"])]
    private $orders;


    #[ORM\ManyToMany(targetEntity: RestaurantTable::class, inversedBy: 'reservations')]
    #[Groups(["reservation:read", "reservation:write", "order:read"])]
    private $restaurantTable;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(["reservation:read", "reservation:write", "restaurantTable:read", "order:read"])]
    private $name;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->restaurantTable = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setReservation($this);
        }
        dump($order);
        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getReservation() === $this) {
                $order->setReservation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getRestaurantTable(): Collection
    {
        return $this->restaurantTable;
    }

    public function addRestaurantTable(RestaurantTable $restaurantTable): self
    {
        if (!$this->restaurantTable->contains($restaurantTable)) {
            $this->restaurantTable[] = $restaurantTable;
        }

        return $this;
    }

    public function removeRestaurantTable(RestaurantTable $restaurantTable): self
    {
        $this->restaurantTable->removeElement($restaurantTable);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
