<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass:MenuRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['menu:write']],
    normalizationContext: ['groups' => ['menu:read']],
)]
#[ApiFilter(BooleanFilter::class, properties: ['isActive'])]
class Menu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type:"integer")]
    #[Groups(["menu:read", "menu:write", "ingredientQuantities:read", "order:read", "restaurantTable:read"])]
    private $id;

    #[ORM\Column(type:"string", length:255)]
    #[Groups(["menu:read", "menu:write", "ingredientQuantities:read", "order:read", "restaurantTable:read"])]
    private $name;

    #[ORM\Column(type:"float")]
    #[Groups(["menu:read", "menu:write", "order:read", "restaurantTable:read"])]
    private $price;

    #[ORM\ManyToMany(targetEntity: Food::class, mappedBy:"menus")]
    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $foods;

    #[ORM\Column(type: 'boolean')]
    #[Groups(["menu:read", "menu:write", "ingredientQuantities:read", "order:read"])]
    private $isActive = true;

    #[ORM\OneToMany(mappedBy: 'menu', targetEntity: Order::class)]
    private $orders;

    // Filter from food if almost one is true
    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isRedMeat;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isWhiteMeat;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isFish;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isVegeteble;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isStarchy;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isDairy;

    #[Groups(["menu:read", "menu:write", "order:read"])]
    private $isPeanut;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->foods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getFoods(): Collection
    {
        return $this->foods;
    }

    public function addFood(Food $food): self
    {
        if (!$this->foods->contains($food)) {
            $this->foods[] = $food;
            $food->addMenu($this);
        }

        return $this;
    }

    public function removeFood(Food $food): self
    {
        if ($this->foods->removeElement($food)) {
            $food->removeMenu($this);
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function getIsRedMeat(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsRedMeat())
                return true;
        }

        return false;
    }

    public function getIsWhiteMeat(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsWhiteMeat())
                return true;
        }

        return false;
    }

    public function getIsFish(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsFish())
                return true;
        }

        return false;
    }

    public function getIsVegeteble(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsVegeteble())
                return true;
        }

        return false;
    }

    public function getIsStarchy(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsStarchy())
                return true;
        }

        return false;
    }

    public function getIsDairy(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsDairy())
                return true;
        }

        return false;
    }

    public function getIsPeanut(): bool
    {
        foreach ($this->getFoods() as $food){
            if ($food->getIsPeanut())
                return true;
        }

        return false;
    }
}
