<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
#[ApiResource(
    denormalizationContext: ['groups' => ['order:write']],
    normalizationContext: ['groups' => ['order:read']],
)]
#[ApiFilter(SearchFilter::class, properties: ['reservation.id' => 'iexact'])]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["reservation:read", "restaurantTable:read", "order:write", "order:read"])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Reservation::class, inversedBy: 'orders', cascade: ["persist"])]
    #[Groups(["order:write", "order:read"])]
    private $reservation;

    #[ORM\ManyToOne(targetEntity: Menu::class, inversedBy: 'orders', cascade: ["persist"])]
    #[Groups(["reservation:read", "restaurantTable:read", "order:write", "order:read"])]
    private $menu;

    #[ORM\ManyToOne(targetEntity: Food::class, inversedBy: 'orders', cascade: ["persist"])]
    #[Groups(["reservation:read", "restaurantTable:read", "order:write", "order:read"])]
    private $food;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(["reservation:read", "restaurantTable:read", "order:write", "order:read"])]
    private $comment;

    // A => Annulé, C => en cours, P => pret
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(["reservation:read", "restaurantTable:read", "order:write", "order:read"])]
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getFood(): ?Food
    {
        return $this->food;
    }

    public function setFood(?Food $food): self
    {
        $this->food = $food;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
