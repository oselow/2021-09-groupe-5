import router from "../router";
import axios from "axios";

export default function axiosOverlay(config, token = true) {

    let temp = axios.defaults.headers.common["Authorization"];
    if (!token) {
        delete axios.defaults.headers.common["Authorization"];
    }

    let res = new Promise((resolve, reject) => {
        axios(config)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                if (error.response && error.response.status === 401) {
                    router.push({name: 'login'});
                }

                if (error.response && error.response.status === 403) {
                    console.log('ERREUR 403')
                }

                reject(error)
            })
    });

    if (!token) {
        axios.defaults.headers.common["Authorization"] = temp;
    }

    return res
}
