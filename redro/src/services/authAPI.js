import Vue from 'vue'
import axios from 'axios'
import {actionsReservation} from "../store/reservation";
import {actionsUser} from "../store/user";

const storeAccount = Vue.observable({
    token: null,
    currentReservationId: null,
});

async function logout() {
    window.localStorage.removeItem("authToken")

    delete axios.defaults.headers.common["Authorization"]
    storeAccount.token = null

    resetReservation()
}

async function resetReservation() {
    window.localStorage.removeItem('currentReservationId')
    storeAccount.currentReservationId = null
}


function authenticateUser(credentials) {
    let data = JSON.stringify({
        'username': credentials.username,
        'password': credentials.password,
    })

    let config = {
        method: 'post',
        url: process.env.VUE_APP_API_URL + '/authentication_token',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    }

    delete axios.defaults.headers.common["Authorization"];

    return axios(config)
        .then(response => {
            return response.data.token
        })
        .then(token => {
            window.localStorage.setItem('authToken', token)
            storeAccount.token = token

            // On prévient axios qu'on aura maintenant un header par défaut sur toutes nos futures requêtes HTTP
            axios.defaults.headers.common["Authorization"] = "Bearer " + token
            return true
        })
}


async function authenticateCustomer(credentials) {
    let res = await actionsReservation.reservationsOfCustomer(credentials)

    return res
}

function setCurrentReservation(reservation) {
    storeAccount.currentReservationId = reservation[0].id
    window.localStorage.setItem('currentReservationId', reservation[0].id)
}

function haveReservation(){
    //A faire de manière plus complète et secure
    /*return (
        this.storeAccount.currentReservationId !== null
        && this.$moment(this.storeAccount.currentReservation.startDate).utc().isBefore(this.$moment().add(15,'minutes').utc())
        && this.$moment(this.storeAccount.currentReservation.endDate).utc().isAfter(this.$moment().add(-15,'minutes').utc())
    );*/

    return this.storeAccount.currentReservationId !== null
}

function isAdmin(){
    return actionsUser.getRoles() && actionsUser.getRoles().includes("ROLE_GESTIONNAIRE")
}

function isConnected(){
    return (storeAccount.token !== null) || (storeAccount.currentReservationId !== null);
}

export default {
    authenticateUser,
    authenticateCustomer,
    logout,
    setCurrentReservation,
    storeAccount,
    haveReservation,
    isAdmin,
    isConnected,
    resetReservation
}
