import Vue from "vue"
import axiosOverlay from "../services/axiosOverlay";

export const storeTable = Vue.observable({
    tables: null,
});

export const actionsTable = {

}

export const mutationsTable = {
    loadTablesFromAPI() {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/restaurant_tables.json?isActive=true'
        };

        return axiosOverlay(config)
            .then(function (response) {
                storeTable.tables = response.data
            })
            .catch(function (error) {
                console.log(error);
            });

    },

}
