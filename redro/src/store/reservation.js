import Vue from "vue"
import axiosOverlay from "../services/axiosOverlay";

export const storeReservation = Vue.observable({
    reservations: null,
    currentReservations: null
});

export const actionsReservation = {
    /**
     * @param {{name: null, tableNumber: null}} credentials
     */
    async reservationsOfCustomer(credentials) {
        return mutationsReservation.loadCurrentReservationsOfCustomerFromAPI(credentials)
    },

    deleteReservationFromAPI(reservationId) {
        let config = {
            method: 'delete',
            url: process.env.VUE_APP_API_URL + '/reservations/' + reservationId
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                if (storeReservation.reservations != null)
                    storeReservation.reservations = storeReservation.reservations.filter(reservation => reservation.id !== reservationId)
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },

    postReservation(payload){
        let config = {
            method: 'post',
            url: process.env.VUE_APP_API_URL + '/reservations',
            data: payload
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                mutationsReservation.loadReservationsFromAPI()
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },
}

export const mutationsReservation = {
    loadReservationsFromAPI() {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/reservations.json'
        };

        return axiosOverlay(config)
            .then(function (response) {
                storeReservation.reservations = response.data
            })
            .catch(function (error) {
                console.log(error);
            });

    },
    loadReservationsFromAPIBetweenDate(start,end) {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/reservations.json?startDate[after]=' + start + '&?endDate[before]' + end
        };

        let res = axiosOverlay(config)
            .then(function (response) {
                storeReservation.reservations = response.data
            })
            .catch(function (error) {
                console.log(error);
            });

        return res
    },
    loadCurrentReservationsFromAPI(){
        let now = new Date();
        now.setMinutes(now.getMinutes() - 15);

        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/reservations.json?startDate[before]=' + now + '&?endDate[after]' + now
        };

        return axiosOverlay(config)
            .then(function (response) {
                storeReservation.currentReservations = response.data
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    loadCurrentReservationsOfCustomerFromAPI(credentials){
        let start = new Date();
        let end = new Date();
        start.setMinutes(start.getMinutes() + 15);
        end.setMinutes(end.getMinutes() - 15);

        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/reservations.json?name=' + credentials.name + '&restaurantTable.id=' + credentials.tableNumber + '&startDate[before]=' + start + '&?endDate[after]' + end
        };

        return axiosOverlay(config, false)
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                console.log(error);
                return null
            });
    },


}
