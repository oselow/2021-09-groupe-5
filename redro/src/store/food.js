import Vue from "vue"
import axiosOverlay from "../services/axiosOverlay";

export const storeFood = Vue.observable({
    foods: null
});

export const actionsFood = {

}

export const mutationsFood = {
    loadFoodFromAPI() {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/food'
        };

        return axiosOverlay(config, false)
            .then(function (response) {
                storeFood.foods = response.data["hydra:member"]
            })
            .catch(function (error) {
                console.log(error);
            });

    }
}