import Vue from "vue"
import axiosOverlay from "../services/axiosOverlay";

export const storeOrder = Vue.observable({
    orders:{},
    order:{}
});

export const actionsOrder = {
    postOrdersIntoAPI(payload) {
        let config = {
            method: 'post',
            url: process.env.VUE_APP_API_URL + '/orders',
            data: payload            
        };

        return axiosOverlay(config, false)
            .then(function () {
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });

    },
    deleteOrdersFromAPI(orderId) {
        let config = {
            method: 'delete',
            url: process.env.VUE_APP_API_URL + '/orders/' + orderId
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                if (storeOrder.orders != null)
                    storeOrder.orders = storeOrder.orders.filter(order => order.id !== orderId)
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },
    cancelOrdersFromAPI(orderId) {
        let config = {
            method: 'patch',
            url: process.env.VUE_APP_API_URL + '/orders/' + orderId,
            data: {status: 'A'}
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                if (storeOrder.orders != null){
                    storeOrder.orders  = storeOrder.orders .map(order => {
                        if (order.id === orderId) {
                            return {...order, status: 'A'};
                        }

                        return order;
                    });
                }
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },
    readyOrdersFromAPI(orderId) {
        let config = {
            method: 'patch',
            url: process.env.VUE_APP_API_URL + '/orders/' + orderId,
            data: {status: 'P'}
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                if (storeOrder.orders != null){
                    storeOrder.orders  = storeOrder.orders .map(order => {
                        if (order.id === orderId) {
                            return {...order, status: 'P'};
                        }

                        return order;
                    });
                }
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },

    inProgressOrdersFromAPI(orderId) {
        let config = {
            method: 'patch',
            url: process.env.VUE_APP_API_URL + '/orders/' + orderId,
            data: {status: 'C'}
        };

        return axiosOverlay(config, false)
            .then(function () {
                //update local
                if (storeOrder.orders != null){
                    storeOrder.orders  = storeOrder.orders .map(order => {
                        if (order.id === orderId) {
                            return {...order, status: 'C'};
                        }

                        return order;
                    });
                }
                return true;
            })
            .catch(function (error) {
                console.log(error);
                return false;
            });
    },
    /*async postOrder (context, data) {
        await process.env.VUE_APP_API_URL.post('/orders.json?isActive=true', data);
      },*/

}

export const mutationsOrder = {
    loadOrdersFromAPI() {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/orders.json?isActive=true'
        };

        return axiosOverlay(config)
            .then(function (response) {
                storeOrder.orders = response.data
            })
            .catch(function (error) {
                console.log(error);
            });

    },

    loadOrderForReservationFromAPI(id) {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/orders.json?isActive=true&reservation.id='+id
        };

        return axiosOverlay(config)
            .then(function (response) {
                storeOrder.orders = response.data
            })
            .catch(function (error) {
                console.log(error);
            });

    },

    setCurrentOrder(state, data){
        storeOrder.order = data;
    },

    setCurrentOrderList(state, data) {
        storeOrder.orders = data
    }
}

