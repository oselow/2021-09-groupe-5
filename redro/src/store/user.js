import Vue from "vue"
import authAPI from "../services/authAPI";
import jwt_decode from "jwt-decode";

export const storeUser = Vue.observable({
    defaultToken: null,
    reservation: null
});

export const actionsUser = {
    jwt() {
        return window.localStorage.getItem("authToken")
    },
    getDecodedToken() {
        if (!this.jwt())
            return null
        try {
            return jwt_decode(this.jwt())
        } catch (e) {
            console.log(e)
        }
    },
    getRoles() {
        if (!this.getDecodedToken())
            return null
        return this.getDecodedToken().roles
    },
}

export const mutationsUser = {
    loadTokenForCustomer() {
        let data = {
            'username': "admin",
            'password': "test",
        }

        authAPI.authenticateUser(data)
    }
}
