import Vue from "vue"
import axiosOverlay from "../services/axiosOverlay";

export const storeMenu = Vue.observable({
    menus: null
});

export const actionsMenu = {

}

export const mutationsMenu = {
    loadMenusFromAPI() {
        let config = {
            method: 'get',
            url: process.env.VUE_APP_API_URL + '/menus?isActive=true'
        };

        return axiosOverlay(config, false)
            .then(function (response) {
                storeMenu.menus = response.data["hydra:member"]
            })
            .catch(function (error) {
                console.log(error);
            });

    }
}
