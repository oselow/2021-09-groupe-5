import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Menu from '../views/Menu.vue'
import Order from '../views/Order.vue'
import Reservation from '../views/Reservation.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    name: 'menu',
    component: Menu
  },
  {
    path: '/order',
    name: 'order',
    component: Order
  },
  {
    path: '/reservations',
    name: 'reservation',
    component: Reservation
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
