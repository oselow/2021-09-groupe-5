import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import moment from 'moment-timezone'

moment.tz.setDefault('Europe/Paris')
moment.locale('fr')
import datePicker from 'vue-bootstrap-datetimepicker';
Vue.use(datePicker);
Vue.prototype.$moment = moment


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

new Vue({
  created() {
    const html = document.documentElement // returns the html tag
    html.setAttribute('lang', 'fr')
  },
  router,
  render: h => h(App)
}).$mount('#app')
