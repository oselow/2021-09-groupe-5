# 2021-09-groupe-5

# TUTO :
https://youtu.be/vLcC8X-huhc

# DEMO :
https://youtu.be/2onrU0Sco2I

# Prérequis pour VUEJS (uniquement pour dev) :

Installer node via
```shell
wget -qO- https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get update -y && sudo apt install -y nodejs
```

Installer npm via
```shell
sudo apt-get update -y && sudo apt-get install npm
```

Installer cli-service via
```shell
sudo npm install -g @vue/cli-service
```

# Deploiement avec Docker
## Lancement
Pour deployer les container, se placer à la racine du projet puis lancer la commande :

```shell
docker-compose up -d
```

Pour rentrer dans le container de symfony et intéragir avec, lancer:

```shell
docker-compose exec php /bin/bash
```

## Configuration Symfony

###JWT

Avant d'utiliser l'api, il faut générer les clefs en lancant le container php et en tapant :

```shell
php bin/console lexik:jwt:generate-keypair
chmod -R 755 config/jwt
```

Si il n'y a aucun utilisateur, vous pouvez l'ajouter manuellement en BDD. Pour le mdp, utilisez la commande suivant dans
le container php pour le générer :

```shell
bin/console s:h <mdp>
```

Puis exécuter la requête SQL :
```sql
INSERT INTO `user`(`email`, `username`, `password`, `roles`) VALUES ('<mail>','<username>','<mdp>', '[]')
```

### .env.local
Initiliser le .env.local dans le dossier api/ qui ressemblera à :

```
DATABASE_URL="mysql://<user>:<mdp>@<ip>:<port>/<db>"

JWT_PASSPHRASE=<pass>
```

Ensuite, si vous êtes en mode dev entrer dans le container php puis executer

```shell
composer install
```

En cas de deploiement en prod executer plutot

```shell
composer install --no-dev
```

Ne pas oublier la section Données

# Lancer VUEJS en mode dev
Se placer dans le projet vuejs et lancer
```shell
sudo npm run serve
```

# Lancer l'API en mode dev
Suivre la section docker mais faire la dernière etape pour un mode "dev"


# Données

Pour mettre à jour la structure de la bdd, se mettre dans le container symfony et lancer les commandes :

```shell
php bin/console doctrine:migration:diff
php bin/console doctrine:migration:migrate
```
---
ATTENTION! Il y a un bug avec les migrations qui necessite d'ajouter le code suivant à la fin du fichier de migration :

```injectablephp
public function isTransactional(): bool
{
    return false;
}
```
---
Pour générer les fausses données, executer :

```shell
php bin/console doctrine:fixtures:load
```

(les utilisateurs ont, par defaut comme mot de passe "test" dans les fixtures et comme username "test0", "test1", "test2', ... , "test9")

# Postman :

### Init :

Pour lancer les requêtes postman, créer une variable d'environnement nommé "ip_api_projet" qui définira l'ip ou l'url de
l'api (ex : http://127.0.0.1:8080)

### Export :

Pour exporter la collection pour des tests ou toute autre raison, exporter la collection dans le fichier prevu à cet
effet dans le dossier "postman" et faire de même pour les variables

# Deploiement GCP
## GCP :
```shell
sudo docker run -ti -e CLOUDSDK_CONFIG=/config/mygcloud -v `pwd`/mygcloud:/config/mygcloud -v `pwd`:/certs  google/cloud-sdk:alpine /bin/bash
```
[Temp] A cause d'un soucis de version, forcer une version à l'aide de :
```shell
gcloud components update --version 365.0.0
```

Se placer dans le dossier du projet
`````shell
cd /certs
`````
Config du projet :
```shell
gcloud auth login
gcloud config set project <projectID> #redro-342115 
gcloud config set compute/zone <zone> #europe-west1-b
gcloud config set compute/region <zone> #europe-west1
```

## Database :
Premierement, Exporter la BDD dans un fichier sql.

Créer la base de données :
```shell
gcloud sql instances create <instanceName> --zone=<zone> --authorized-networks=0.0.0.0/0 #europe-west1-b
gcloud sql users set-password root --host=% --instance <instanceName> --password <pass>
gcloud sql databases create <db_name> --instance=<instanceName>
```

Ensuite, récuperez l'ip public (retour de commande de création) et la placer dans le .env.local de l'api ainsi que les log que vous venez de definir

(Vous pouvez importer votre database via l'interface graphique) :
Creer un bucket sur gcp
Importer le ficher SQL

SQL -> Importer -> Choisir le fichier crée et choisir la base de données que crée.
<hr>

Attention, penser à changer votre nom de registry docker dans les 2 fichiers YAML prévu a cet effet et les commandes à venir.

Ne pas oublier de se log à DockerHub

Activer kubernetes avec le lien suivant : https://console.cloud.google.com/flows/enableapi?apiid=artifactregistry.googleapis.com,container.googleapis.com&_ga=2.46561636.231817401.1645525705-679368739.1637149469&_gac=1.45726422.1645536471.CjwKCAiAsNKQBhAPEiwAB-I5zQ_QuqeDIN4OFz6uZkHEtcwHVibTtx_4ez1XsTOyF9QZY76wdrBo2BoCFQsQAvD_BwE

## Kubernetes :
Créer un cluster :
```shell
gcloud components install kubectl
gcloud container clusters create <cluster_name>
```

## API :
### Build api :
```shell
#Se placer dans le dossier api/ (sur la machine local pas le docker)
docker build --no-cache -t <registryname>/redro-api .
docker tag <registryname>/redro-api <registryname>/redro-api
docker push <registryname>/redro-api
```

### 1er deploiement:
```shell
gcloud compute addresses create redro-api-ip
gcloud compute addresses describe redro-api-ip
```
Placer l'ip récupéré avec la précédente commande dans le fichier "kube-redro-api.yaml" puis exécutez les commandes suivantes :
```shell
##### Version CLI (depreciated)
#kubectl create deployment <deployment-name> --image=<registryname>/redro-api
#kubectl expose deployment <deployment-name> --name=<service-name> --type=LoadBalancer --load-balancer-ip="<ip>" --port 80 --target-port 80
kubectl create -f kube-redro-api.yaml
kubectl get services
```
Récupérer l'ip de l'api et la mettre dans le .env du vuejs

### Mise à jour du deploiement:
```shell
kubectl apply -f kube-redro-api.yaml
kubectl get services
```

## Vuejs :
### Build vuejs :
```shell
#Se placer dans le dossier redro/ (sur la machine local pas le docker)
docker build --no-cache -t <registryname>/redro-vuejs .
docker tag <registryname>/redro-vuejs <registryname>/redro-vuejs
docker push <registryname>/redro-vuejs
```
### 1er deploiement:
```shell
gcloud compute addresses create redro-vuejs-ip
gcloud compute addresses describe redro-vuejs-ip
```
Placer l'ip récupéré avec la précédente commande dans le fichier "kube-redro-vuejs.yaml" puis exécutez les commandes suivantes :
```shell
kubectl create -f kube-redro-vuejs.yaml
kubectl get services
##### Version CLI (depreciated)
#kubectl create deployment <deployment-name> --image=<registryname>/redro-vuejs
#kubectl expose deployment <deployment-name> --name=<service-name> --type=LoadBalancer --port 80 --target-port 8080
```

Récupérez l'ip et c'est boooon

### Mise à jour du deploiement:
```shell
kubectl apply -f kube-redro-vuejs.yaml
kubectl get services
```

# INFO URLS :

Toutes le post d'un user demande d'être log, les autres sont (pour le moment) accessible à tout le monde. Pour les POSTs
ou autres requêtes, regarder la collection POSTMAN pour voir un exemple

# INFO GIT :

Si les commandes git passent pas, mettre un chmod 777 sur le dossier mysql (à resoudre plusproprement plus tard)

#
# DEMO DEPLOIEMENT GCP :
https://www.youtube.com/watch?v=xMHNhDPfB0o

# Time code des commandes :
1) https://youtu.be/xMHNhDPfB0o?t=1

2) https://youtu.be/xMHNhDPfB0o?t=5

3&4) https://youtu.be/xMHNhDPfB0o?t=14 (cert & auth)

5-8) https://youtu.be/xMHNhDPfB0o?t=41 (config project)
#

DATABASE:

6) https://youtu.be/xMHNhDPfB0o?t=103 (Database instance)

7) https://youtu.be/xMHNhDPfB0o?t=135 (set password)

8) https://youtu.be/xMHNhDPfB0o?t=165 (Create Database)

9) https://youtu.be/xMHNhDPfB0o?t=172 (Edition .env.local (ip, user, password database gcp))
#

Bucket:

10) https://youtu.be/xMHNhDPfB0o?t=229 (Création d'un bucket)

11) https://youtu.be/xMHNhDPfB0o?t=260 (alimentation du bucket avec le fichier d'exportation de la base de donnée redro)

12) https://youtu.be/xMHNhDPfB0o?t=297 (Import bucket) 
#

Edition yaml:

13) https://youtu.be/xMHNhDPfB0o?t=297 (modification du fichier kubectl-redro-api.yml, mise a jour du repository)

14) https://youtu.be/xMHNhDPfB0o?t=297 (modification du fichier kubectl-redro-vuejs.yml, mise a jour du repository)
#
Activation accès a APIs:

15) https://youtu.be/xMHNhDPfB0o?t=372
#
Installation Kubectl:

16) https://youtu.be/xMHNhDPfB0o?t=385

17) https://youtu.be/xMHNhDPfB0o?t=396 (Création d'un clusters)
#
Build & Push docker images

18) https://youtu.be/xMHNhDPfB0o?t=412
#
Creation d'adresse IP API:

19) https://youtu.be/xMHNhDPfB0o?t=451

#
Recuperation de l'IP crée: 

20) https://youtu.be/xMHNhDPfB0o?t=456 

21) https://youtu.be/xMHNhDPfB0o?t=473 (modification de l'ip dans kubeclt-redro-api.yaml)

22) https://youtu.be/xMHNhDPfB0o?t=495 (Modification du .env replacement d'IP par celle recuperée)

23) https://youtu.be/xMHNhDPfB0o?t=508 (kubectl apply)

#
Build & Push docker images

24) https://youtu.be/xMHNhDPfB0o?t=528

# 
Creation d'IP pour le front :

25) https://youtu.be/xMHNhDPfB0o?t=583

26) https://youtu.be/xMHNhDPfB0o?t=596 (Inserer l'IP recuperée dans kubectl-redro-vuejs.yaml)

27) https://youtu.be/xMHNhDPfB0o?t=607 (Kubeclt create)

28) https://youtu.be/xMHNhDPfB0o?t=614 (Recuperation de l'IP du front, It's Work !!!)